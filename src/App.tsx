import React from "react";
import { Route, Routes } from "react-router-dom";
import Main from "./routes/Main";
import WhichWordle from "./routes/WhichWordle";

const App: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Main />} />
      <Route path="/whichwordle" element={<WhichWordle />} />
    </Routes>
  );
};

export default App;
