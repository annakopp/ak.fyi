module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        eblue: "#03009C",
        egray: "#D7D9CE",
        egreen: "#526760",
        epink: "#BC4B51",
      },
    },
  },
  plugins: [],
};
